## Overview

This Python application saves files to the BackBlaze B2 system.

This application is written using Python 3.6 and later.

## Motivation

...

## How to Run

In the top-level directory:

    $ source venv/bin/activate
    $ pip install --editable .
    $ b2_client --help

## Configuration

There are (3) key parameters that should be defined in b2_parameters.json:

| Parameter                 | Description                                                |
| ------------------------- | ---------------------------------------------------------- |
| b2_account_id             | Account ID for your BackBlaze B2 Account                   |
| b2_authentication_token   | Authentication Token for your BackBlaze B2 Account         |
| b2_default_bucket         | Default bucket name when using the BackBlaze B2 API        |

## Unit Testing

    $ pytest






