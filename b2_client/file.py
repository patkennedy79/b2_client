import abc
import hashlib
import os


class File(abc.ABC):
    """
    Abstract Base Class (ABC) for a file.

    This class can represent a file that has been uploaded to B2 or a file on the local computer.
    """
    def __init__(self, filename: str):
        self.filename = self.calculate_filename(filename)
        self.contentType = self.calculate_content_type(filename)
        self.contentSha1 = None
        self.contentLength = self.calculate_sha1()

    def __eq__(self, other):
        return self.contentSha1 == other.contentSha1

    def __repr__(self):
        return f'<{self.filename}, {self.contentSha1}>'

    def print_file_metadata(self):
        print(f'file name: {self.filename}')
        print(f'contentType: {self.contentType}')
        print(f'contentSha1: {self.contentSha1}')
        print(f'contentLength: {self.contentLength}')
        print()

    @abc.abstractmethod
    def calculate_filename(self, filename: str) -> str:
        """Returns the filename of the file."""

    @abc.abstractmethod
    def calculate_filename_with_path(self, filename: str) -> str:
        """Returns the filename with the full path of the file."""

    @staticmethod
    def calculate_sha1(filename_with_full_path):
        """
        Returns the 40-character hex SHA1 checksum of the specified file

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            SHA-1: the 40-character hex SHA1 checksum
            Length: the length of the file (in Bytes)
        """
        h = hashlib.sha1()
        size_in_bytes = 0

        # open file for reading in binary mode
        with open(filename_with_full_path, 'rb') as file:
            # read only 1024 bytes at a time
            chunk = file.read(1024)

            # loop till the end of the file
            while chunk:
                h.update(chunk)
                size_in_bytes += len(chunk)

                # read only 1024 bytes at a time
                chunk = file.read(1024)

        return h.hexdigest(), str(size_in_bytes)

    @staticmethod
    def calculate_content_type(filename) -> str:
        """
        Calculated the content type of a file to use for uploading to B2.

        Supported types:
           - Images (*.jpg, *.jpeg, *.png): 'image/jpeg'
           - Videos (*.mov): 'video/quicktime'

        Args:
            filename: name (without the full path) of the file

        Returns:
            Content type of the specified files
        """
        _, file_extension = os.path.splitext(filename)

        if file_extension.lower() in ('.jpg', '.jpeg', '.png'):
            return 'image/jpeg'
        elif file_extension.lower() in ('.mov'):
            return 'video/quicktime'
        else:
            return ''
