import abc
import hashlib
import os


class File(abc.ABC):
    """
    Abstract Base Class (ABC) for a file.

    This class can represent a file that has been uploaded to B2 or a file on the local computer.
    """
    def __init__(self, filename: str):
        self.filename = self.calculate_filename(filename)
        self.contentType = None
        self.contentSha1 = None
        self.contentLength = None

    def __eq__(self, other):
        return self.contentSha1 == other.contentSha1

    def __repr__(self):
        return f'<{self.filename}, {self.contentSha1}>'

    def print_file_metadata(self):
        print(f'file name: {self.filename}')
        print(f'contentType: {self.contentType}')
        print(f'contentSha1: {self.contentSha1}')
        print(f'contentLength: {self.contentLength}')
        print()

    @abc.abstractmethod
    def calculate_filename(self, filename: str) -> str:
        """Returns the filename of the file."""

    @abc.abstractmethod
    def calculate_filename_with_path(self, filename: str) -> str:
        """Returns the filename with the full path of the file."""

    @staticmethod
    def calculate_sha1(filename_with_full_path):
        """
        Returns the 40-character hex SHA1 checksum of the specified file

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            SHA-1: the 40-character hex SHA1 checksum
            Length: the length of the file (in Bytes)
        """
        h = hashlib.sha1()
        size_in_bytes = 0

        # open file for reading in binary mode
        with open(filename_with_full_path, 'rb') as file:
            # read only 1024 bytes at a time
            chunk = file.read(1024)

            # loop till the end of the file
            while chunk:
                h.update(chunk)
                size_in_bytes += len(chunk)

                # read only 1024 bytes at a time
                chunk = file.read(1024)

        return h.hexdigest(), str(size_in_bytes)

    @staticmethod
    def calculate_content_type(filename_with_full_path) -> str:
        """
        Calculated the content type of a file to use for uploading to B2.

        Supported types:
           - Images (*.jpg, *.jpeg, *.png): 'image/jpeg'
           - Videos (*.mov): 'video/quicktime'

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            Content type of the specified files
        """
        filename, file_extension = os.path.splitext(filename_with_full_path)

        if file_extension.lower() in ('.jpg', '.jpeg', '.png'):
            return 'image/jpeg'
        elif file_extension.lower() in ('.mov'):
            return 'video/quicktime'
        else:
            return ''


class BackupFile:
    """
    Class for keeping track of a file that has been uploaded to B2 or should be uploaded to B2.

    The filename that is used for uploading to B2 cannot contain spaces (' ') or commas (','), so both
    the filename and filename_with_upload_path are stored with the spaces and commas changed to underscores ('_').
    The filename_with_full_path is not modified in this way, as this would prevent actually accessing
    the file on the local disk.

    Attributes:
        filename (str): the name of the file without the path
        filename_with_full_path (str): the name of the file with the full path on the local disk
        filename_with_upload_path (str): the name of the file with the path used for uploading to B2
        action (str): One of "start", "upload", "hide", "folder".
                       - "start" means that a large file has been started, but not finished or canceled.
                       - "upload" means a file that was uploaded to B2.
                       - "hide" means a file version marking the file as hidden, so that it will not show up in b2_list_file_names.
                       - "folder" is used to indicate a virtual folder when listing files.
        contentType (str): the type of the file to use when uploading to B2.  Supported types:
                       - Images (*.jpg, *.jpeg, *.png): 'image/jpeg'
                       - Videos (*.mov): 'video/quicktime'
        contentSha1 (str): the SHA-1 of the full file
        contentLength (str): the length (in Bytes) of the file
        uploadable (bool): flag indicating if the file is of the type (image, video) to be uploaded to B2
        uploaded_to_b2 (bool): flag indicating if the file has been uploaded to B2
    """

    def __init__(self, file_with_full_path='', file_with_upload_path='', action='', contentType='', contentSha1='',
                 contentLength=''):
        # When an instance of BackupFile is created, it is either:
        #   1. a file found on the local disk - specifies file_with_full_path
        #   2. a file already uploaded to B2 - specifies file_with_upload_path
        if file_with_full_path != '':
            self.filename = BackupFile.calculate_filename(file_with_full_path)
            self.filename_with_full_path = file_with_full_path
            self.filename_with_upload_path = BackupFile.calculate_upload_path(self.filename_with_full_path)
            self.action = ''
            self.contentType = BackupFile.calculate_content_type(self.filename_with_full_path)
            self.contentSha1, self.contentLength = BackupFile.calculate_sha1(self.filename_with_full_path)
            self.uploadable = BackupFile.check_file_is_uploadable(self.filename_with_full_path)
            self.uploaded_to_b2 = False
        else:
            path, filename = os.path.split(file_with_upload_path)
            self.filename = filename
            self.filename_with_full_path = ''
            self.filename_with_upload_path = file_with_upload_path
            self.action = action
            self.contentType = contentType
            self.contentSha1 = contentSha1
            self.contentLength = contentLength
            self.uploadable = True
            self.uploaded_to_b2 = True

    def __eq__(self, other):
        return self.contentSha1 == other.contentSha1

    def __repr__(self):
        return f'<{self.filename}, {self.contentSha1}>'

    @staticmethod
    def calculate_filename(input_file) -> str:
        """
        Returns the filename of the specified path

        Since the filename that is used for uploading to B2 cannot contain spaces (' ') or commas (','),
        the filename that is returned has the spaces and commas changed to underscores ('_').

        Args:
            input_file: path to a file

        Returns:
            Filename: filename with the spaces and commas changed to underscores ('_').
        """
        path, filename = os.path.split(input_file)
        return BackupFile.format_for_b2(filename)

    @staticmethod
    def calculate_sha1(filename_with_full_path):
        """
        Returns the 40-character hex SHA1 checksum of the specified file

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            SHA-1: the 40-character hex SHA1 checksum
            Length: the length of the file (in Bytes)
        """
        h = hashlib.sha1()
        size_in_bytes = 0

        # open file for reading in binary mode
        with open(filename_with_full_path, 'rb') as file:
            # read only 1024 bytes at a time
            chunk = file.read(1024)

            # loop till the end of the file
            while chunk:
                h.update(chunk)
                size_in_bytes += len(chunk)

                # read only 1024 bytes at a time
                chunk = file.read(1024)

        return h.hexdigest(), str(size_in_bytes)

    @staticmethod
    def check_file_is_uploadable(filename_with_full_path) -> bool:
        """
        Returns a flag indicating if the file should be uploaded to B2.

        File extensions that at considered valid:
          - Images (*.jpg, *.jpeg, *.JPG, *.JPEG, *.png, *.PNG)
          - Videos (*.mov, *.MOV)

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            True if the file should be uploaded, False if it should not be uploaded (ie. not an image/video)
        """
        filename, file_extension = os.path.splitext(filename_with_full_path)

        if file_extension.lower() in ('.jpg', '.jpeg', '.png', '.mov'):
            return True
        else:
            return False

    @staticmethod
    def calculate_upload_path(filename_with_full_path) -> str:
        """
        Returns the path to upload the file to B2 with.

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            Path for uploading the file to B2.
        """
        file_with_upload_path = ''
        picture_folder_found = False
        tokens = filename_with_full_path.rstrip().split('/')
        for token in tokens:
            if not picture_folder_found:
                if token == 'Pictures':
                    picture_folder_found = True
                    # print(f'1token: {token}')
                    file_with_upload_path += token
            else:
                if not token == '':
                    # print(f'2token: {token}')
                    file_with_upload_path += '/' + token

        # print(f'Files would be uploaded to {file_with_upload_path}')
        return BackupFile.format_for_b2(file_with_upload_path)

    @staticmethod
    def calculate_content_type(filename_with_full_path) -> str:
        """
        Calculated the content type of a file to use for uploading to B2.

        Supported types:
           - Images (*.jpg, *.jpeg, *.png): 'image/jpeg'
           - Videos (*.mov): 'video/quicktime'

        Args:
            filename_with_full_path: file with the full path on the local disk

        Returns:
            Content type of the specified files
        """
        filename, file_extension = os.path.splitext(filename_with_full_path)

        if file_extension.lower() in ('.jpg', '.jpeg', '.png'):
            return 'image/jpeg'
        elif file_extension.lower() in ('.mov'):
            return 'video/quicktime'
        else:
            return ''

    @staticmethod
    def format_for_b2(input_string) -> str:
        """
        Formats the input string to work with B2

        This method changes the following characters in the specified string:
            - space (' ') --> underscore ('_')
            - comma (',') --> underscore ('_')
            - ampersand ('&') --> 'and'
            - colon (':') --> underscore ('_')
            - open square bracket ('[') --> underscore ('_')
            - close square bracket ('[') --> underscore ('_')
            - e with accent ('è') --> e ('e')
            - single quote ("'") --> underscore ('_')

        Args:
            input_string: string to format

        Returns:
            Formatted string
        """
        return input_string.replace(' ', '_') \
                           .replace(',', '_') \
                           .replace('&', 'and') \
                           .replace(':', '_') \
                           .replace('[', '_') \
                           .replace(']', '_') \
                           .replace('è', 'e') \
                           .replace("'", "_")

    def print_file_metadata(self):
        print(f'file name: {self.filename}')
        print(f'action: {self.action}')
        print(f'contentType: {self.contentType}')
        print(f'contentSha1: {self.contentSha1}')
        print(f'contentLength: {self.contentLength}')
        print()
