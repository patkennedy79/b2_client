import click
import os
import time
from b2_client.b2_session import B2Session
from b2_client.backup_directory import Directory


MONTHS = [
    '01',  # January
    '02',  # February
    '03',  # March
    '04',  # April
    '05',  # May
    '06',  # June
    '07',  # July
    '08',  # August
    '09',  # September
    '10',  # October
    '11',  # November
    '12'   # December
]


# ----------------
# Helper Functions
# ----------------

def get_picture_directory(year: int) -> str:
    """Returns the path to the picture folder based on the specified year."""
    if year < 2005 or year > 2024:
        return ''
    return os.path.join('/Users/patrickkennedy/Pictures/', str(year))


# ------------
# CLI Commands
# ------------

@click.group()
@click.option('--verbose', is_flag=True)
def cli(verbose):
    """
    Script for working with the BackBlaze B2 API
    """
    if verbose:
        click.echo('We are in verbose mode!')


@cli.command()
def check_authentication():
    """
    Check authentication settings for B2
    """
    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    click.secho(f'************* B2 Base URL************', fg='blue')
    click.secho(f'Base URL from B2: {b2session.get_b2_authentication_token()}', fg='blue')


@cli.command()
def list_buckets():
    """
    Retrieve all buckets from B2 using the b2_list_buckets command
    """
    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    b2session.retrieve_all_buckets()

    click.secho(f'************* List of Buckets ************', fg='blue')
    for bucket in b2session.buckets:
        click.secho(f'Bucket name: {bucket.bucket_name}, id: {bucket.bucket_id}', fg='blue')


@cli.command()
@click.argument('new_bucket_name')
def create_bucket(new_bucket_name):
    """
    Create a new bucket on B2 using the b2_create_bucket command

    This command requires that the name of the new bucket be specified as an argument.
    """
    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    b2session.create_new_bucket(new_bucket_name)


@cli.command()
@click.option('--directory', type=click.Path(), help='Directory to search for files.')
@click.option('--bucket_name', type=str, help='Bucket name to upload files to.')
def find_files_to_upload(directory, bucket_name):
    """
    Find list of files that would be uploaded (pre-upload check!)
    """
    upload_file_counter = 0

    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    b2session.retrieve_all_buckets()

    click.secho(f'*****************************************************************', fg='blue')
    click.secho(f'Finding files to upload to B2...', fg='blue')
    click.secho(f'Directory: {directory}, Bucket Name: {bucket_name}', fg='blue')

    bucket_id = b2session.get_bucket_id(bucket_name)
    b2_files = b2session.print_files_in_bucket(bucket_id, directory)
    files = b2session.find_files_to_upload(directory)

    for file in files:
        if file not in b2_files and file.uploadable and not file.uploaded_to_b2:
            upload_file_counter += 1

    click.secho(f'Proposed Upload:', fg='blue')
    click.secho(f'\tTotal Files: {upload_file_counter}', fg='blue')


@cli.command()
@click.option('--directory', type=click.Path(), help='Directory containing files to upload.')
@click.option('--bucket_name', type=str, help='Bucket name to upload files to.')
def upload_files(directory, bucket_name):
    """
    Upload files to B2
    """
    upload_counter = 0
    upload_successful_counter = 0

    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    b2session.retrieve_all_buckets()

    click.secho(f'*****************************************************************')
    click.secho(f'Uploading files to B2...')
    click.secho(f'Directory: {directory}, Bucket Name: {bucket_name}')

    bucket_id = b2session.get_bucket_id(bucket_name)
    b2_files = b2session.print_files_in_bucket(bucket_id, directory)
    files = b2session.find_files_to_upload(directory)

    for file in files:
        if file not in b2_files and file.uploadable and not file.uploaded_to_b2:
            upload_counter += 1
            upload_attempt_counter = 0

            while upload_attempt_counter < 6:
                upload_url, upload_authorization_token = b2session.get_upload_url(bucket_id)
                if b2session.upload_file(upload_authorization_token, upload_url, file, (upload_successful_counter + 1), len(files)):
                    upload_successful_counter += 1
                    break
                else:
                    # If the upload failed, sleep for 5.0 seconds and then try again
                    upload_attempt_counter += 1
                    time.sleep(5)

    click.secho(f'Upload Results:', fg='blue')
    click.secho(f'\tSuccessful Uploads: {upload_successful_counter}', fg='green')
    click.secho(f'\tFailed Uploads: {upload_counter-upload_successful_counter}', fg='red')
    click.secho(f'\tTotal Files: {upload_counter}', fg='blue')


@cli.command()
@click.option('--directory', type=click.Path(), help='Directory containing files to list.')
@click.option('--bucket_name', type=str, help='Bucket name on B2.')
def print_files_in_bucket(directory, bucket_name):
    """
    Print the list of files to B2 in the specified bucket and directory
    """
    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    b2session.retrieve_all_buckets()
    bucket_id = b2session.get_bucket_id(bucket_name)

    click.secho(f'***************** FILES IN B2 BUCKET ****************************', fg='blue')
    click.secho(f'B2 Bucket: {bucket_name}, ID: {bucket_id}', fg='blue')
    click.secho(f'Directory: {directory}', fg='blue')

    # Listing of all the files in the B2 bucket for this directory
    b2_files = b2session.print_files_in_bucket(bucket_id, directory)

    backup_directory = Directory(directory)
    backup_directory.files = b2_files
    click.secho(backup_directory.get_printable_table(), fg='black')


@cli.command()
@click.option('--bucket_name', type=str, help='Bucket name on B2.')
@click.option('--year', type=int, help='Year (2005, 2006, ..., 2024) that the pictures were taken.')
@click.option('--month', type=int, help='Month (1 - 12) that the pictures were taken.')
def print_picture_directories(bucket_name, year, month):
    """
    Print the list of picture directories for the specified year and month
    """
    b2session = B2Session()
    if not b2session.check_for_valid_configuration():
        exit(1)
    b2session.get_b2_authentication_token()
    b2session.retrieve_all_buckets()
    bucket_id = b2session.get_bucket_id(bucket_name)

    base_dir = get_picture_directory(year)

    click.secho(f'***************** LIST OF PICTURE FILES *************************', fg='blue')
    click.secho(f'B2 Bucket: {bucket_name}, ID: {bucket_id}', fg='blue')
    click.secho(f'Directory: {base_dir}', fg='blue')

    all_entries = os.listdir(base_dir)
    directories = []
    for entry in all_entries:
        if os.path.isdir(os.path.join(base_dir, entry)):
            if entry.startswith(str(year) + '-' + MONTHS[month - 1]):
                directories.append(os.path.join(base_dir, entry))

    for directory in sorted(directories):
        click.secho()
        click.secho(f"Picture Directory: {directory}", fg='blue')
        click.secho('-' * (len("Picture Directory: ") + len(directory)))

        # Listing of all the files in the B2 bucket for this directory
        b2_files = b2session.print_files_in_bucket(bucket_id, directory)

        # Listing of all the files on the local drive, including checking if
        # the file has been uploaded to B2
        backup_directory = Directory(os.path.join(base_dir, directory))
        backup_directory.check_if_files_uploaded_to_b2(b2_files)
        click.secho(backup_directory.get_printable_table(), fg='black')
