from dataclasses import dataclass, field
from typing import List
from b2_client.backupfile import BackupFile
from prettytable import PrettyTable
import os


@dataclass
class Directory:
    """Class representing a directory containing files to back up to B2."""
    path: str
    files: List[BackupFile] = field(default_factory=list)

    def __post_init__(self):
        """Collect all the files in the directory."""
        files = os.listdir(self.path)
        for file in sorted(files):
            self.files.append(BackupFile(file_with_full_path=os.path.join(self.path, file)))

    def print_all_files(self):
        """Print all the files in a table format."""
        print(self.get_printable_table())

    def get_printable_table(self) -> PrettyTable:
        """Returns a PrettyTable object that can be printed."""
        table = PrettyTable()
        table.field_names = ['File', 'SHA1', 'Uploadable', 'Uploaded to B2']
        for file in self.files:
            table.add_row([file.filename, file.contentSha1, file.uploadable, file.uploaded_to_b2])
        return table

    def check_if_files_uploaded_to_b2(self, uploaded_files: []):
        """Check if the files have been uploaded to B2 based on the specified list of uploaded files."""
        for file in self.files:
            if file.uploadable and file in uploaded_files:
                file.uploaded_to_b2 = True
