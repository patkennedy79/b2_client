from dataclasses import dataclass


@dataclass
class Bucket:
    """
    Class for keeping track of a file that has been uploaded to B2 or should be uploaded to B2.

    The storage on BackBlaze B2 is grouped into buckets. Each bucket is a container that holds files.
    You can think of buckets as the top-level folders in your B2 account. There is no limit to the number
    of files in a bucket, but there is a limit of 100 buckets per account.

    Documentation for the B2 Buckets:
        https://www.backblaze.com/b2/docs/buckets.html

    Attributes:
        bucket_name (str): Name of the bucket
        bucket_id (str): ID of the bucket
        bucket_type (str): Type of the bucket, either 'allPrivate' or 'allPublic'

    """
    bucket_name: str
    bucket_id: str
    bucket_type: str = 'allPrivate'
