import os
import logging
import requests
import base64
from b2_client.bucket import Bucket
from b2_client.backupfile import BackupFile


URL_AUTHENTICATION = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
BUCKET_TYPE = "allPrivate"  # Either allPublic or allPrivate


class B2Session:
    """
    Class for keeping track of session interacting with the B2 API.

        * Authentication Token - needed for subsequent API calls to B2
        * API URL - needed as the base URL for subsequent API calls to B2
    """

    def __init__(self):
        self.logger = self.configure_logger()
        self.valid_configuration_file = False
        self.b2_authentication_string = ''
        self.b2_default_bucket = ''
        self.b2_account_id = ''
        self.authorizationToken = ''
        self.apiUrl = ''
        self.buckets = []

    @staticmethod
    def configure_logger():
        # Create a custom logger
        logger = logging.getLogger(__name__)

        # Create handlers
        f_handler = logging.FileHandler('b2_client.log')

        # Create formatters and add it to handlers
        f_format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        f_handler.setFormatter(f_format)

        # Add handlers to the logger and set the logging level
        logger.addHandler(f_handler)
        logger.setLevel(logging.DEBUG)
        return logger

    def check_for_valid_configuration(self) -> bool:
        """Check for the required environment variables.

        This method checks that the following environment variables are defined:
            * B2_ACCOUNT_ID
            * B2_AUTHENTICATION_TOKEN
            * B2_DEFAULT_BUCKET
        """
        if 'B2_ACCOUNT_ID' not in os.environ or 'B2_AUTHENTICATION_TOKEN' not in os.environ or 'B2_DEFAULT_BUCKET' not in os.environ:
            print('ERROR! One or more required environment variables is not defined!')
            print('    1. B2_ACCOUNT_ID')
            print('    2. B2_AUTHENTICATION_TOKEN')
            print('    3. B2_DEFAULT_BUCKET')
            return False

        self.b2_account_id = os.environ['B2_ACCOUNT_ID']
        self.b2_authentication_string = os.environ['B2_AUTHENTICATION_TOKEN']
        self.b2_default_bucket = os.environ['B2_DEFAULT_BUCKET']
        return True

    def get_b2_authentication_token(self):
        """
        Returns the API URL to use as the base for further API calls

        This function requires that the B2_AUTHENTICATION_STRING be set to a string of the format:
            b'<account_id>:<application_key>'

        Documentation for the b2_list_buckets API operation:
            https://www.backblaze.com/b2/docs/b2_authorize_account.html

        Returns:
            Authentication Token - needed for subsequent API calls to B2
            API URL - needed as the base URL for subsequent API calls to B2
        """
        auth_string_bytes = self.b2_authentication_string.encode('ascii')
        auth_string_base64_bytes = base64.b64encode(auth_string_bytes)
        basic_auth_string = 'Basic ' + auth_string_base64_bytes.decode('ascii')
        headers = {'Authorization': basic_auth_string}

        r = requests.get(URL_AUTHENTICATION, headers=headers)

        self.logger.debug(f'*************Response from B2 for b2_authorize_account command*************')
        self.logger.debug(f'URL: {r.url}')
        self.logger.debug(f'Status Code: {r.status_code}')
        self.logger.debug(f'Headers: {r.headers}')
        self.logger.debug(f'Response Text: {r.text}')

        self.logger.info('************* Authentication Token from Backblaze B2 ************')

        if r.status_code == 200:
            response_data = r.json()
            self.logger.info(f'auth token: {response_data["authorizationToken"]}')
            self.logger.info(f'api url: {response_data["apiUrl"]}')
            self.logger.info(f'download url: {response_data["downloadUrl"]}')
            self.authorizationToken = response_data['authorizationToken']
            self.apiUrl = response_data['apiUrl']
        else:
            print()
            print(f'ERROR getting the Authentication Token from B2!')
            self.logger.error(f'ERROR getting the Authentication Token from B2!')
            self.logger.error(f'\tStatus code: {r.status_code}')
            self.logger.error(f'\tResponse text: {r.text}')
            self.authorizationToken = ''
            self.apiUrl = ''

        return self.apiUrl

    def retrieve_all_buckets(self):
        """
        Retrieve all buckets from B2 using the b2_list_buckets API operation.

        Documentation for the b2_list_buckets API operation:
            https://www.backblaze.com/b2/docs/b2_create_bucket.html

        Args:
            auth_token (str): Authentication token needed in header for call to b2_list_buckets
            base_url (str): The first part of the URL needed for calling b2_list_buckets

        Returns:
            list: List of all buckets (defined as Bucket objects) in B2 account
        """
        self.buckets = []
        headers = {'Authorization': self.authorizationToken}
        json_data = {'accountId': self.b2_account_id}
        url = self.apiUrl + '/b2api/v2/b2_list_buckets'
        r = requests.post(url, headers=headers, json=json_data)

        self.logger.debug(f'*************Response from B2 for b2_list_buckets command*************')
        self.logger.debug(f'URL: {r.url}')
        self.logger.debug(f'Status Code: {r.status_code}')
        self.logger.debug(f'Headers: {r.headers}')
        self.logger.debug(f'Response Text: {r.text}')

        if r.status_code == 200:
            response_data = r.json()
            self.logger.info(f'************* List of Buckets ************')
            for bucket in response_data['buckets']:
                self.logger.info(f'bucket name: {bucket["bucketName"]}')
                self.logger.info(f'bucket id: {bucket["bucketId"]}')
                self.logger.info(f'bucket type: {bucket["bucketType"]}')
                self.buckets.append(Bucket(bucket['bucketName'], bucket['bucketId'], bucket['bucketType']))
        else:
            print()
            print(f'ERROR retrieving buckets from B2!')
            self.logger.error(f'ERROR retrieving buckets from B2!')
            self.logger.error(f'\tStatus code: {r.status_code}')
            self.logger.error(f'\tResponse text: {r.text}')

        return

    def create_new_bucket(self, bucket_name):
        """
        Creates a new bucket on B2

        Documentation for the b2_list_buckets API operation:
            https://www.backblaze.com/b2/docs/b2_create_bucket.html

        Args:
            auth_token (str): Authentication token needed in header for call to b2_list_buckets
            base_url (str): The first part of the URL needed for calling b2_list_buckets
            bucket_name (str): Name of the new bucket to create on B2

        Returns:
            True if the bucket was created, False if the bucket was not created (ie. an error occurred)
        """
        if bucket_name is None or bucket_name == '':
            print('ERROR!  Bucket name was not specified!  Not creating a new bucket...')
            self.logger.error('ERROR!  Bucket name was not specified!  Not creating a new bucket...')
            return False

        headers = {'Authorization': self.authorizationToken}
        json_data = {'accountId': self.b2_account_id,
                     'bucketName': bucket_name,
                     'bucketType': BUCKET_TYPE}
        url = self.apiUrl + '/b2api/v2/b2_create_bucket'
        r = requests.post(url, headers=headers, json=json_data)

        self.logger.debug(f'*************Response from B2 for b2_create_bucket command*************')
        self.logger.debug(f'URL: {r.url}')
        self.logger.debug(f'Status Code: {r.status_code}')
        self.logger.debug(f'Headers: {r.headers}')
        self.logger.debug(f'Response Text: {r.text}')

        self.logger.info('************* Creating New Bucket ************')

        if r.status_code == 200:
            response_data = r.json()
            self.logger.info(f'New bucket was created successfully on B2!')
            self.logger.info(f'\tBucket id: {response_data["bucketId"]}')
            self.logger.info(f'\tBucket name: {response_data["bucketName"]}')
            self.logger.info(f'\tBucket type: {response_data["bucketType"]}')
            print(f'New bucket {response_data["bucketName"]} was created successfully on B2!')
            return True
        else:
            print()
            print(f'ERROR creating a new bucket on B2!')
            self.logger.error(f'ERROR creating a new bucket on B2!')
            self.logger.error(f'\tStatus code: {r.status_code}')
            self.logger.error(f'\tResponse text: {r.text}')

        return False

    def get_bucket_id(self, bucket_name):
        """
        Return the ID of the specified bucket
        """
        for bucket in self.buckets:
            self.logger.debug(f'Bucket... name: {bucket.bucket_name}, id: {bucket.bucket_id}')
            if bucket_name == bucket.bucket_name:
                self.logger.info(f'Found bucket!  Name: {bucket.bucket_name}, id: {bucket.bucket_id}')
                return bucket.bucket_id

        return ''

    def print_files_in_bucket(self, bucket_id, directory):
        """
        Retrieve all files from B2 using the specified bucket with the specified directory
        """
        files = []
        request_count = 1
        prefix = BackupFile.calculate_upload_path(directory)
        startFileName = ''
        self.logger.info(f'Printing files in bucket... prefix: {prefix}, startFileName: {startFileName}')

        while startFileName is not None:
            headers = {'Authorization': self.authorizationToken}
            json_data = {'bucketId': bucket_id,
                         'prefix': prefix,
                         'startFileName': startFileName,
                         'maxFileCount': 100}
            url = self.apiUrl + '/b2api/v2/b2_list_file_names'
            self.logger.debug(f'Sending b2_list_file_names request #{request_count} with prefix: {prefix} and startFileName: {startFileName}')
            request_count += 1
            r = requests.post(url, headers=headers, json=json_data)

            self.logger.debug(f'*************Response from B2 for b2_list_file_names command*************')
            self.logger.debug(f'URL: {r.url}')
            self.logger.debug(f'Status Code: {r.status_code}')
            self.logger.debug(f'Headers: {r.headers}')
            self.logger.debug(f'Response Text: {r.text}')

            response_data = r.json()

            for file in response_data['files']:
                self.logger.debug(f'File in bucket: {file["fileName"]}')
                files.append(BackupFile(file_with_upload_path=file['fileName'],
                                        action=file['action'],
                                        contentType=file['contentType'],
                                        contentSha1=file['contentSha1'],
                                        contentLength=file['contentLength']))

            startFileName = response_data['nextFileName']
            self.logger.debug(f'nextFileName: {startFileName}')

        self.logger.info(f'Total files: {len(files)}')
        return files

    def find_files_to_upload(self, base_directory):
        total_number_of_files = 0
        files = []

        if base_directory is None:
            base_directory = os.getcwd()
            self.logger.warning(f'No directory specified... using current directory: {base_directory}!')

        for dirpath, dirnames, filenames in os.walk(base_directory, topdown=True):
            for name in filenames:
                backup_file = BackupFile(file_with_full_path=os.path.join(dirpath, name))
                if backup_file.uploadable:
                    total_number_of_files += 1
                files.append(backup_file)
                self.logger.info(f'Local file: {os.path.join(dirpath, name)}')

        return files

    def get_upload_url(self, bucket_id):
        """
        Get the URL for uploading a file to B2 using the b2_get_upload_url API operation.

        Per the B2 documentation, the process of getting an upload URL involves targeting
        a single storage pod in a Backblaze data center.  While this makes the uploads efficient,
        it does mean that getting the upload URL may fail if the pod is full or busy.  Therefore,
        it is recommended to try to retrieve the upload URL up to 5 times before failing.

        Documentation for the b2_get_upload_url API operation:
            https://www.backblaze.com/b2/docs/b2_get_upload_url.html

        Args:
            auth_token (str): Authentication token needed in header for call to b2_list_buckets
            base_url (str): The first part of the URL needed for calling b2_list_buckets
            bucket_id (str): The bucket ID to upload to

        Returns:
            url (str): URL to upload a file to
            upload_auth_token (str): Authorization token needed to upload a file
        """
        attempts = 0
        headers = {'Authorization': self.authorizationToken}
        json_data = {'bucketId': bucket_id}
        url = self.apiUrl + '/b2api/v2/b2_get_upload_url'

        while attempts < 5:
            r = requests.post(url, headers=headers, json=json_data)
            attempts += 1

            self.logger.debug(f'*************Response from B2 for b2_get_upload_url command*************')
            self.logger.debug(f'URL: {r.url}')
            self.logger.debug(f'Status Code: {r.status_code}')
            self.logger.debug(f'Headers: {r.headers}')
            self.logger.debug(f'Response Text: {r.text}')

            if r.status_code == 200:
                self.logger.info(f'************* URL for Uploading Files ************')
                response_data = r.json()
                self.logger.info(f'Upload URL: {response_data["uploadUrl"]}')
                return response_data['uploadUrl'], response_data['authorizationToken']

        return

    def upload_file(self, upload_auth_token, url, file, file_number, number_of_files):
        """
        Upload a file to B2
        :return:
        """
        headers = {'Authorization': upload_auth_token,
                   'X-Bz-File-Name': file.filename_with_upload_path,
                   'Content-Type': file.contentType,
                   'X-Bz-Content-Sha1': file.contentSha1,
                   'Content-Length': file.contentLength}

        self.logger.info(f'************* Upload File ************')
        self.logger.info(f'file to upload: {file.filename_with_full_path}')

        with open(file.filename_with_full_path, 'rb') as file_to_send:
            r = requests.post(url, headers=headers, data=file_to_send.read())

        self.logger.debug(f'*************Response from B2 for b2_upload_file command*************')
        self.logger.debug(f'URL: {r.url}')
        self.logger.debug(f'Status Code: {r.status_code}')
        self.logger.debug(f'Headers: {r.headers}')
        self.logger.debug(f'Response Text: {r.text}')

        json_data = r.json()

        # Check the sha1 returned from the server matches the calculated sha1 of the file
        if r.status_code == 200 and file.contentSha1 == json_data['contentSha1'] and json_data['action'] == 'upload':
            self.logger.info(f'[{file_number}/{number_of_files}] File ({file.filename_with_full_path}) was uploaded successfully!')
            print(f'[{file_number}/{number_of_files}] File ({file.filename_with_full_path}) was uploaded successfully!')
            return True
        else:
            print(f'ERROR! File ({file.filename_with_full_path}) was not uploaded.')
            self.logger.error(f'ERROR! File ({file.filename_with_full_path}) was not uploaded.')
            self.logger.error(f'\tStatus code: {r.status_code}')
            self.logger.error(f'\tText: {r.text}')

        return False
