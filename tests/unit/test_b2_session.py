"""
This file (test_b2_session.py) contains the unit tests for testing the B2Session class in b2_session.py.
"""
import pytest
import os
import requests
from b2_client.b2_session import B2Session
from b2_client.bucket import Bucket


def test_get_b2_authentication_token_fail(monkeypatch):
    def mock_get(url, headers):
        mock_response = requests.Response()
        mock_response.status_code = 404
        return mock_response

    monkeypatch.setattr(requests, 'get', mock_get)

    new_session = B2Session()
    new_session.get_b2_authentication_token()
    assert new_session.authorizationToken == ''
    assert new_session.apiUrl == ''


def test_get_b2_authentication_token_success(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 200
            self.text = '''
                            {'accountId': '1234',
                             'apiUrl': 'https://api000.backblazeb2.com',
                             'authorizationToken': '5678',
                             'downloadUrl': 'https://f000.backblazeb2.com'}
                        '''
            self.url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
            self.headers = {'blaa': '9ABC'}

        def json(self):
            return {'accountId': '1234',
                    'apiUrl': 'https://api000.backblazeb2.com',
                    'authorizationToken': '5678',
                    'downloadUrl': 'https://f000.backblazeb2.com'}

    def mock_get(url, headers):
        return MockResponse()

    monkeypatch.setattr(requests, 'get', mock_get)

    new_session = B2Session()
    new_session.check_for_valid_configuration()
    new_session.get_b2_authentication_token()
    assert new_session.authorizationToken == '5678'
    assert new_session.apiUrl == 'https://api000.backblazeb2.com'


def test_get_b2_authentication_token_failure(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 401
            self.text = '''
                            {'error': 'bad'}
                        '''
            self.url = 'BAD'
            self.headers = {}

        def json(self):
            return {'error': 'bad'}

    def mock_get(url, headers):
        return MockResponse()

    monkeypatch.setattr(requests, 'get', mock_get)

    new_session = B2Session()
    new_session.check_for_valid_configuration()
    new_session.get_b2_authentication_token()
    assert new_session.authorizationToken == ''
    assert new_session.apiUrl == ''


def test_retrieve_all_buckets_success(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 200
            self.text = '''
                            {'buckets':
                                [{'bucketName': 'bucket1234',
                                  'bucketId': '987654321',
                                  'bucketType': 'privateBucket'},
                                 {'bucketName': 'bucket1234',
                                  'bucketId': '987654321',
                                  'bucketType': 'privateBucket'}]}
                        '''
            self.url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
            self.headers = {'blaa': '9ABC'}

        def json(self):
            return {'buckets':
                        [{'bucketName': 'bucket1234',
                         'bucketId': '987654321',
                         'bucketType': 'privateBucket'},
                        {'bucketName': 'bucket1234',
                         'bucketId': '987654321',
                         'bucketType': 'privateBucket'}]
            }

    def mock_post(url, headers, json):
        return MockResponse()

    monkeypatch.setattr(requests, 'post', mock_post)

    new_session = B2Session()
    new_session.retrieve_all_buckets()

    assert len(new_session.buckets) == 2
    for bucket in new_session.buckets:
        assert bucket.bucket_name == 'bucket1234'
        assert bucket.bucket_id == '987654321'
        assert bucket.bucket_type == 'privateBucket'


def test_retrieve_all_buckets_success_no_buckets(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 200
            self.text = '''
                            {'buckets': []}
                        '''
            self.url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
            self.headers = {'blaa': '9ABC'}

        def json(self):
            return {'buckets': []}

    def mock_post(url, headers, json):
        return MockResponse()

    monkeypatch.setattr(requests, 'post', mock_post)

    new_session = B2Session()
    new_session.retrieve_all_buckets()
    assert len(new_session.buckets) == 0


def test_retrieve_all_buckets_failure(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 401
            self.text = '''
                                {'error': 'bad'}
                            '''
            self.url = 'BAD'
            self.headers = {}

        def json(self):
            return {'error': 'bad'}

    def mock_post(url, headers, json):
        return MockResponse()

    monkeypatch.setattr(requests, 'post', mock_post)

    new_session = B2Session()
    new_session.retrieve_all_buckets()
    assert len(new_session.buckets) == 0


def test_create_new_bucket_success(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 200
            self.text = '''
                            {'bucketName': 'bucket1234',
                             'bucketId': '987654321',
                             'bucketType': 'privateBucket'}
                        '''
            self.url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
            self.headers = {'blaa': '9ABC'}

        def json(self):
            return {'bucketName': 'bucket1234',
                    'bucketId': '987654321',
                    'bucketType': 'privateBucket'
            }

    def mock_post(url, headers, json):
        return MockResponse()

    monkeypatch.setattr(requests, 'post', mock_post)

    new_session = B2Session()
    assert new_session.create_new_bucket('TestBucket1') == True


def test_create_new_bucket_no_bucket_name():
    new_session = B2Session()
    assert new_session.create_new_bucket(None) == False

    new_session = B2Session()
    assert new_session.create_new_bucket('') == False


def test_create_new_bucket_failure(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 401
            self.text = '''
                            {'error': 'bad'}
                        '''
            self.url = 'BAD'
            self.headers = {}

        def json(self):
            return {'error': 'bad'}

    def mock_post(url, headers, json):
        return MockResponse()

    monkeypatch.setattr(requests, 'post', mock_post)

    new_session = B2Session()
    assert new_session.create_new_bucket('TestBucket1') == False


def test_get_bucket_id_success():
    bucket1 = Bucket('bucket1', '123', 'Private')
    bucket2 = Bucket('bucket2', '234', 'Private')
    bucket3 = Bucket('bucket3', '452', 'Public')
    new_session = B2Session()
    new_session.buckets = [bucket1, bucket2, bucket3]
    assert new_session.get_bucket_id('bucket1') == '123'
    assert new_session.get_bucket_id('bucket2') == '234'
    assert new_session.get_bucket_id('bucket3') == '452'


def test_get_bucket_id_failure():
    bucket1 = Bucket('bucket1', '123', 'Private')
    bucket2 = Bucket('bucket2', '234', 'Private')
    bucket3 = Bucket('bucket3', '452', 'Public')
    new_session = B2Session()
    new_session.buckets = [bucket1, bucket2, bucket3]
    assert new_session.get_bucket_id('bucket4') == ''
    assert new_session.get_bucket_id('blaa') == ''
