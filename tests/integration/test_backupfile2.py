import pytest
from b2_client.backupfile import BackupFile
"""
This file (test_backupfile2.py) contains the unit tests for testing the BackupFile class in the backupfile.py file.
"""


def test_valid_b2_backupfile():
    """
    GIVEN a valid path returned from B2
    WHEN .
    THEN check the BackupFile instance created
    """
    file1 = BackupFile(file_with_upload_path='Pictures/FoodPictures/IMG_001.jpg',
                       action='upload',
                       contentType='image/jpeg',
                       contentSha1='ABCDABCDABCDABCDABCD',
                       contentLength='232324')
    assert file1.filename == 'IMG_001.jpg'
    assert file1.filename_with_upload_path == 'Pictures/FoodPictures/IMG_001.jpg'
    assert file1.contentType == 'image/jpeg'
    assert file1.contentSha1 == 'ABCDABCDABCDABCDABCD'
    assert file1.uploadable == True
    assert file1.uploaded_to_b2 == True
    assert file1.action == 'upload'
    assert file1.contentLength == '232324'
    assert file1.filename_with_full_path == ''


def test_valid_local_file(test_file):
    """
    GIVEN a valid path on the local hard drive
    WHEN .
    THEN check the BackupFile instance created
    """
    file1 = BackupFile(file_with_full_path=str(test_file))
    assert file1.filename == 'hello.txt'
    assert file1.filename_with_full_path != ''
    assert file1.filename_with_upload_path == ''
    assert file1.contentType == ''
    assert file1.contentSha1 == '040f06fd774092478d450774f5ba30c5da78acc8'
    assert file1.uploadable == False
    assert file1.uploaded_to_b2 == False
    assert file1.contentLength == '7'
    assert file1.action == ''


def test_get_upload_path_valid():
    """
    GIVEN a valid path from the file system
    WHEN the path includes the /Pictures/ folder
    THEN check the upload path returned
    """
    file_with_path = '/Users/patrickkennedy/Pictures/FoodPictures/IMG_001.jpg'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/FoodPictures/IMG_001.jpg'

    file_with_path = '/Users/patrickkennedy/Pictures/FoodPictures/Cookies/IMG_002.JPeG'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/FoodPictures/Cookies/IMG_002.JPeG'


def test_get_upload_path_invalid():
    """
    GIVEN a valid path from the file system
    WHEN the path does not include the /Pictures/ folder
    THEN check the upload path returned is the same path
    """
    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_001.jpg'
    assert BackupFile.calculate_upload_path(file_with_path) == ''

    file_with_path = '/Users/patrickkennedy/Movies/FoodPictures/Cookies/IMG_002.JPeG'
    assert BackupFile.calculate_upload_path(file_with_path) == ''


def test_calculate_filename():
    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_001.jpg'
    assert BackupFile.calculate_filename(file_with_path) == 'IMG_001.jpg'

    file_with_path = '/Users/patrickkennedy/Movies/FoodPictures/Cookies/IMG_002.JPeG'
    assert BackupFile.calculate_filename(file_with_path) == 'IMG_002.JPeG'

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_001'
    assert BackupFile.calculate_filename(file_with_path) == 'IMG_001'


def test_calculate_filename_with_replacement():
    # Test that the following characters are properly changed to support B2:
    #   - space(' ') --> underscore('_')
    #   - comma(',') --> underscore('_')
    #   - ampersand('&') --> 'and'
    file_with_path = '/data/backup/pictures/more_pictures.121231/my&_ sdfds,kdsf@#'
    assert BackupFile.calculate_filename(file_with_path) == 'myand__sdfds_kdsf@#'

    file_with_path = '/data/backup/pictures/more_pictures.121231/my&_ sdfds,kdsf@#.qt'
    assert BackupFile.calculate_filename(file_with_path) == 'myand__sdfds_kdsf@#.qt'


def test_check_file_is_uploadable_valid():
    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_001.jpg'
    assert BackupFile.check_file_is_uploadable(file_with_path) == True

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_002.jpeg'
    assert BackupFile.check_file_is_uploadable(file_with_path) == True

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_003.jPg'
    assert BackupFile.check_file_is_uploadable(file_with_path) == True

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_004.PnG'
    assert BackupFile.check_file_is_uploadable(file_with_path) == True

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_005.MOv'
    assert BackupFile.check_file_is_uploadable(file_with_path) == True

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_006.mov'
    assert BackupFile.check_file_is_uploadable(file_with_path) == True


def test_check_file_is_uploadable_invalid():
    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_001.gif'
    assert BackupFile.check_file_is_uploadable(file_with_path) == False

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_002.qt'
    assert BackupFile.check_file_is_uploadable(file_with_path) == False

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_003.jpeg1'
    assert BackupFile.check_file_is_uploadable(file_with_path) == False

    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_004.JPGA'
    assert BackupFile.check_file_is_uploadable(file_with_path) == False


def test_calculate_upload_path():
    file_with_path = '/Users/patrickkennedy/Pictures/FoodPictures/IMG_001.jpeg'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/FoodPictures/IMG_001.jpeg'

    file_with_path = '/Users/patrickkennedy/Pictures/FoodPictures/More_Pictures/IMG_002.jpeg'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/FoodPictures/More_Pictures/IMG_002.jpeg'

    file_with_path = '/Users/patrickkennedy/Pictures/12323/IMG_003.jpeg'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/12323/IMG_003.jpeg'


def test_calculate_upload_path_movies():
    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_001.mov'
    assert BackupFile.calculate_upload_path(file_with_path) == ''


def test_calculate_upload_path_with_replacement():
    file_with_path = '/Users/patrickkennedy/Pictures/Food Pictures, More Pictures & More/IMG_001.jpeg'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/Food_Pictures__More_Pictures_and_More/IMG_001.jpeg'


def test_calculate_content_type():
    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_001.jpeg'
    assert BackupFile.calculate_content_type(file_with_path) == 'image/jpeg'

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_002.JPeG'
    assert BackupFile.calculate_content_type(file_with_path) == 'image/jpeg'

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_003.pNg'
    assert BackupFile.calculate_content_type(file_with_path) == 'image/jpeg'

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_004.png'
    assert BackupFile.calculate_content_type(file_with_path) == 'image/jpeg'

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_005.mov'
    assert BackupFile.calculate_content_type(file_with_path) == 'video/quicktime'

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_006.MoV'
    assert BackupFile.calculate_content_type(file_with_path) == 'video/quicktime'


def test_calculate_content_type_invalid():
    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_001.jpeg2'
    assert BackupFile.calculate_content_type(file_with_path) == ''

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_002.qt'
    assert BackupFile.calculate_content_type(file_with_path) == ''

    file_with_path = '/Users/patrickkennedy/Movies/2018/IMG_003.MOV2'
    assert BackupFile.calculate_content_type(file_with_path) == ''
