import pytest
import requests
from requests import Response
from b2_client.backupfile import BackupFile
from b2_client.b2_session import B2Session
"""
This file (test_b2_client.py) contains the unit tests for the b2_client.py file.
"""


def test_get_upload_path_valid():
    """
    GIVEN a valid path from the file system
    WHEN the path includes the /Pictures/ folder
    THEN check the upload path returned
    """
    file_with_path = '/Users/patrickkennedy/Pictures/FoodPictures/IMG_001.jpg'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/FoodPictures/IMG_001.jpg'

    file_with_path = '/Users/patrickkennedy/Pictures/FoodPictures/Cookies/IMG_002.JPeG'
    assert BackupFile.calculate_upload_path(file_with_path) == 'Pictures/FoodPictures/Cookies/IMG_002.JPeG'


def test_get_upload_path_invalid():
    """
    GIVEN a valid path from the file system
    WHEN the path does not include the /Pictures/ folder
    THEN check the upload path returned is the same path
    """
    file_with_path = '/Users/patrickkennedy/Picture/FoodPictures/IMG_001.jpg'
    assert BackupFile.calculate_upload_path(file_with_path) == ''

    file_with_path = '/Users/patrickkennedy/Movies/FoodPictures/Cookies/IMG_002.JPeG'
    assert BackupFile.calculate_upload_path(file_with_path) == ''


def test_get_b2_authentication_token_fail(monkeypatch):
    def mock_get(url, headers):
        mock_response = Response()
        mock_response.status_code = 404
        return mock_response

    monkeypatch.setattr(requests, 'get', mock_get)
    b2session = B2Session()
    b2session.check_for_valid_configuration()
    assert b2session.get_b2_authentication_token() == ''


def test_get_b2_authentication_token_success(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 200
            self.text = '''
                            {'accountId': '1234',
                             'apiUrl': 'https://api000.backblazeb2.com',
                             'authorizationToken': '5678',
                             'downloadUrl': 'https://f000.backblazeb2.com'}
                        '''
            self.url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
            self.headers = {'blaa': '9ABC'}

        def json(self):
            return {'accountId': '1234',
                    'apiUrl': 'https://api000.backblazeb2.com',
                    'authorizationToken': '5678',
                    'downloadUrl': 'https://f000.backblazeb2.com'}

    def mock_get(url, headers):
        return MockResponse()

    monkeypatch.setattr(requests, 'get', mock_get)
    b2session = B2Session()
    b2session.check_for_valid_configuration()
    assert b2session.get_b2_authentication_token() == 'https://api000.backblazeb2.com'


def test_get_b2_authentication_token_failure(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 401
            self.text = '''
                            {'error': 'bad'}
                        '''
            self.url = 'BAD'
            self.headers = {}

        def json(self):
            return {'error': 'bad'}

    def mock_get(url, headers):
        return MockResponse()

    monkeypatch.setattr(requests, 'get', mock_get)
    b2session = B2Session()
    b2session.check_for_valid_configuration()
    assert b2session.get_b2_authentication_token() == ''


def test_create_new_bucket_success(monkeypatch):
    class MockResponse(object):
        def __init__(self):
            self.status_code = 200
            self.text = '''
                            {'accountId': '1234',
                             'apiUrl': 'https://api000.backblazeb2.com',
                             'authorizationToken': '5678',
                             'downloadUrl': 'https://f000.backblazeb2.com'}
                        '''
            self.url = 'https://api.backblazeb2.com/b2api/v2/b2_authorize_account'
            self.headers = {'blaa': '9ABC'}

        def json(self):
            return {'accountId': '1234',
                    'apiUrl': 'https://api000.backblazeb2.com',
                    'authorizationToken': '5678',
                    'downloadUrl': 'https://f000.backblazeb2.com'}

    def mock_get(url, headers):
        return MockResponse()

    monkeypatch.setattr(requests, 'get', mock_get)
    b2session = B2Session()
    b2session.check_for_valid_configuration()
    assert b2session.get_b2_authentication_token() == 'https://api000.backblazeb2.com'
