import pytest


@pytest.fixture(scope='session')
def test_file(tmpdir_factory):
    p = tmpdir_factory.mktemp("sub").join("hello.txt")
    p.write("content")
    return p
